require "pi_piper"
include PiPiper

relay1 = PiPiper::Pin.new(:pin => 7, :direction => :out)
relay2 = PiPiper::Pin.new(:pin => 8, :direction => :out)
relay3 = PiPiper::Pin.new(:pin => 15, :direction => :out)
relay4 = PiPiper::Pin.new(:pin => 14, :direction => :out)

relays = [relay1, relay2, relay3, relay4]

loop do
  relays.each do |relay|
    relay.on
  end
  sleep 0.5
  relays.each do |relay|
    relay.off
  end
  sleep 0.5
end
