require "pi_piper"
include PiPiper

relay1 = PiPiper::Pin.new(:pin => 7, :direction => :out)
relay2 = PiPiper::Pin.new(:pin => 8, :direction => :out)
relay3 = PiPiper::Pin.new(:pin => 15, :direction => :out)
relay4 = PiPiper::Pin.new(:pin => 14, :direction => :out)

relays = [relay1, relay2, relay3, relay4]

relays.each do |relay|
  puts relay.to_s + " on!"
  relay.on
  sleep 0.5
end

sleep 0.5

relays.each do |relay|
  puts relay.to_s + " off.."
  relay.off
  sleep 0.5
end
